default: lambda-cbv/lambda-cbv.pdf lambda-cbv-simptyp/lambda-cbv-simptyp.pdf lambda-cbv-latex/lambda-cbv-latex.pdf

lambda-cbv/lambda-cbv.pdf:
	make -C lambda-cbv

lambda-cbv-simptyp/lambda-cbv-simptyp.pdf:
	make -C lambda-cbv-simptyp

lambda-cbv-latex/lambda-cbv-latex.pdf:
	make -C lambda-cbv-latex

clean:
	make -C lambda-cbv 	   clean
	make -C lambda-cbv-simptyp clean
	make -C lambda-cbv-latex   clean
