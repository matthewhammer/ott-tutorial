= Ott =

  PLUM reading group
  Ott intro tutorial
  2013.02.18 -- 13:30

Ott Website: 
  http://www.cl.cam.ac.uk/~pes20/ott/

People: Francesco Zappa Nardelli, Peter Sewell, and Scott Owens (with
  Matthew Parkinson, Gilles Peskine, Tom Ridge, Susmit Sarkar, and Rok
  Strniša)

JFP Paper: 
  http://www.cl.cam.ac.uk/~pes20/ott/ott-jfp.pdf


== Supplemental Ott resources ==

  "Ottalt" LaTeX package:
  Code repo: https://github.com/tov/latex-ottalt
  PDF doc:   http://www.eecs.harvard.edu/~tov/code/latex/ottalt/ottalt.pdf


== Overview ==

We give an overview of what Ott is, and what it is for.

Main points (with quotes from manual):

-- Chief aim: Large language definitions

  "Our main goal is to support work on large programming language
  definitions, where the scale makes it hard to keep a definition
  internally consistent, and hard to keep a tight correspondence
  between a definition and implementations."

-- Common use case: Small calculi

  "We also wish to ease rapid prototyping work with smaller calculi,
  and to make it easier to exchange definitions and definition
  fragments between groups."


Chief benefits:

-- Reduces __syntantic noise__ (compared to LaTeX, proof assistants, etc.)

  "Most simply, the tool can be used to aid completely informal LaTeX
  mathematics.  Here it permits the definition, and terms within
  proofs and exposition, to be written in a clear, editable, ASCII
  notation, without LaTeX noise. It generates good-quality typeset
  output."

-- Does automatic sanity checking

  "By parsing (and so sort-checking) this input, it quickly catches a
  range of simple errors, e.g. inconsistent use of judgement forms or
  metavariable naming conventions. That same input, extended with some
  additional data, can be used to generate formal definitions for Coq,
  HOL, and Isabelle. It should thereby enable a smooth transition
  between use of informal and formal mathematics."


Focus of this tutorial:

-- I assume that your critical path is a LaTeX workflow
-- I will focus on Ott as a practical/civilized tool for producing LaTeX
-- I will ignore features that address a proof assistant work flow

Structure of this tutorial:

-- Introduce basic Ott keywords / concepts
-- Go over examples: 
   1. CBV untyped lambda calculus
   2. Simply-typed extension


== Example 1 : Call-by-value Lambda Calculus ==

(See: lambda-cbv/lambda-cbv.ott)
(Discussion below is organized by the Ott keywords therein)

-- metavar: 

   defines "meta variables" -- variables in meta language (Ott) that
   range over variables in object/term language being defined (e.g.,
   the lambda calculus).

-- grammar: 

   defines a context-free grammar

   associated with productions are _homs_ (homomorphisms, discussed later)

-- subrules:

   declares that one grammar's formal language is a subset of another

-- defns:

   names a block of defn blocks

-- defn:

   defines a named relation by a collection of inference rules

-- inference rules (no keyword):

  premise1
  premise2
  .
  .
  premisen
  -------------- :: rule_name
  conclusion


-- auto-generated grammars:

   ( See also:
   http://www.cl.cam.ac.uk/~pes20/ott/ott_manual_0.21.2.html#htoc8 )

   -- Every defn adds a new case to the judgement grammar
      e.g., 

      judgement ::= ... | Jop

   -- Each premise/conclusion of a rule is a _formula_, which is
      another auto-generated Ott grammar.
   
      judgement <:: formula

   -- Additional cases can be added explicitly, as with any grammar

   -- Optionally (default:false), Ott can -merge grammars and defns

      -- Similiar to the TinkerType system used by Ben Pierce to
         create TAPL


-- terminals:
   
   -- Is another "special" grammar (recall above special grammars,
      "formula" and "judgement")

   -- Is used to define the terminals of the language (those
      grammatical structures that have no substructures).

   -- Pratically, can be used to give rewrite homs for writing LaTeX
      with less syntactic noise



== Example 2: Group Exercise / Demonstration ==

  ( See: lambda-cbv-simptyp/simptyp.ott )

  ** Add to lambda-cbv, the typing judgement for STLC.

     -- Pieces:
        -- grammar: types..?
        -- grammar: base-type values..?
        -- grammar: terminals..?
        -- defns: value and term typing



== Example 3: Further integration into LaTeX workflow ===

  ( See: lambda-cbv-latex/Makefile
         lambda-cbv-latex/main.mng )

  ** Use defined Ott syntaxes within LaTeX source (using quotation + filtering)


