This directory demonstrates how I beautify the (La)TeX output of Ott.

-- Makefile          -- demonstrates the LateX/Ott workflow

-- main.mng          -- LaTeX that contains embedded syntax/definitions, preprocessed by Ott 

-- ottalt.sty        -- redefines some commands used in Ott output
                     -- search for XXX to see one change I made

-- ott-local.tex     -- redefines commands in Ott output
                     -- these tweaks are all simple to read/change

-- includes.tex      -- boilerplate \usepackage{..} code

-- grammar-names.ott -- gives names to the productions defined in Ott 

Other stuff:

-- listproc.sty    -- required by ottalt.sty
-- mathpartir.sty  -- math paragraphs (for typesetting inference rules)
-- ottlayout.sty   -- Not currently used.  Another package to beautify Ott.
